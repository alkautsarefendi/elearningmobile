package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.potyvideo.library.AndExoPlayerView;

import test.alkautsar.elearningmobile.R;

public class DetailVideoMSActivity extends AppCompatActivity {

    public static final String URL_VIDEO = "URL_VIDEO";
    public static final String JUDUL_VIDEO = "JUDUL_VIDEO";
    public static final String DESK_VIDEO = "DESK_VIDEO";
    String linkVideo, getJudulVideo, descVideo;
    private AndExoPlayerView andExoPlayerView;
    LinearLayout header;
    TextView txtJudulVMS, txtDescMS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_video_m_s);

        Intent intent = getIntent();
        linkVideo = intent.getStringExtra(URL_VIDEO);
        getJudulVideo = intent.getStringExtra(JUDUL_VIDEO);
        descVideo = intent.getStringExtra(DESK_VIDEO);
        System.out.println("LinkVIDEO = " + linkVideo);
        System.out.println("JudulVideo = " + getJudulVideo);
        System.out.println("DESK_VIDEO = " + descVideo);

        andExoPlayerView = findViewById(R.id.andExoPlayerView);
        txtJudulVMS = findViewById(R.id.txtJudulVideoMS);
        txtDescMS = findViewById(R.id.txtDeskripsiMS);
        header = findViewById(R.id.header);

        andExoPlayerView.setShowFullScreen(true);
        andExoPlayerView.setSource(linkVideo);
        andExoPlayerView.setPlayWhenReady(true);

        txtJudulVMS.setText(getJudulVideo);
        txtDescMS.setText(descVideo);
    }
}