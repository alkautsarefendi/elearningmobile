package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.potyvideo.library.AndExoPlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.model.ImageMateriModel;
import test.alkautsar.elearningmobile.utils.Utils;

public class DetailMateriVideoActivity extends AppCompatActivity {

    private AndExoPlayerView andExoPlayerView;
    public static final String EXTRA_ID_MATERI = "idMateri";
    public static final String EXTRA_TYPE_MATERI = "typeMateri";
    private String idMateri, json, file, typeMateri;
    LinearLayout header;
    Button btnNextMateriDetail;
    TextView judulVideo, deskripsiVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_materi_video);

        andExoPlayerView = findViewById(R.id.andExoPlayerView);
        header = findViewById(R.id.header);
        judulVideo = findViewById(R.id.txtJudulVideo);
        deskripsiVideo = findViewById(R.id.txtDeskripsi);

        Intent intent = getIntent();
        idMateri = intent.getStringExtra(EXTRA_ID_MATERI);
        typeMateri = intent.getStringExtra(EXTRA_TYPE_MATERI);
        System.out.println("idMateri = " + idMateri);
        System.out.println("typeMateri = " + typeMateri);

        loadMateriDetail();

        int display_mode = getResources().getConfiguration().orientation;
        if (display_mode == Configuration.ORIENTATION_LANDSCAPE) {
            header.setVisibility(View.GONE);
        } else {
            header.setVisibility(View.VISIBLE);
        }

        andExoPlayerView.setShowFullScreen(true);

        btnNextMateriDetail = findViewById(R.id.btnNextMateriDetail);
        btnNextMateriDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                andExoPlayerView.stopPlayer();
                Intent i = new Intent(getApplicationContext(), PostTestActivity.class);
                i.putExtra(EXTRA_ID_MATERI,idMateri);
                i.putExtra(EXTRA_TYPE_MATERI, typeMateri);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        andExoPlayerView.stopPlayer();
    }

    private void loadMateriDetail() {

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        System.out.println("TOKEN = " + token);

        try {
            JSONObject obj = new JSONObject();
            obj.put("materi", idMateri);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "video/bymateri",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            System.out.println("message = " + message);
                            String path = jresp.getString("path");
                            String data = jresp.getString("data");

                            JSONArray dataArray = new JSONArray(data);
                            for (int i = 0; i < dataArray.length(); i++){
                                JSONObject imgJSON = dataArray.getJSONObject(i);
                                String nama = imgJSON.getString("nama");
                                String desc = imgJSON.getString("deskripsi");
                                file = imgJSON.getString("file");

                                System.out.println("NAMA FILE = "+nama);
                                System.out.println("FILE = "+file );

                                judulVideo.setText(nama);
                                deskripsiVideo.setText(desc);

                            }

                            if (dataArray.length() == 0){
                                Utils.toastAlert(DetailMateriVideoActivity.this, "Materi Video tidak ada");
                            }

                            System.out.println("PATH = "+path );
                            String linkVidio = path+file;
                            Log.e("LINK",linkVidio);

                            andExoPlayerView.setSource(linkVidio);
                            andExoPlayerView.setPlayWhenReady(true);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(DetailMateriVideoActivity.this).add(stringRequest);
    }
}