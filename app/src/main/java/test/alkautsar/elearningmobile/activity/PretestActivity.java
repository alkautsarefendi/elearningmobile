package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.adapter.MateriAdapter;
import test.alkautsar.elearningmobile.model.DataMateri;
import test.alkautsar.elearningmobile.model.SoalPretestModel;
import test.alkautsar.elearningmobile.utils.Utils;

public class PretestActivity extends AppCompatActivity {

    public static final String EXTRA_ID_MATERI = "idMateri";
    public static final String EXTRA_JUDUL = "judul";
    public static final String EXTRA_TYPE_MATERI = "typeMateri";
    private String json, jawaban, idMateri, tglMulai, jamMulai, tglSelesai, jamSelesai, jumlahSkor,
            start, stop, lamaWaktu, typeMateri;
    private TextView txtSoal, txtLamaWaktu;
    private Button btnSubmitPretest;
    SoalPretestModel spm;
    private List<SoalPretestModel> list_soal;
    int arrSoal;
    int x;
    int skor = 0;
    ArrayAdapter adapter;
    RadioGroup radioGroup;
    RadioButton rbOpsiA;
    RadioButton rbOpsiB;
    RadioButton rbOpsiC;
    RadioButton rbOpsiD;
    Date d1 = null;
    Date d2 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pretest);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        Intent intent = getIntent();
        idMateri = intent.getStringExtra(EXTRA_ID_MATERI);
        String judul = intent.getStringExtra(EXTRA_JUDUL);
        typeMateri = intent.getStringExtra(EXTRA_TYPE_MATERI);

        System.out.println("idMateri = " + idMateri);
        System.out.println("judul = " + judul);
        System.out.println("typeMateri = " + typeMateri);

        list_soal = new ArrayList<>();

        txtSoal = (TextView) findViewById(R.id.tvSoal);
        txtLamaWaktu = findViewById(R.id.txtLamaWaktu);
        radioGroup = findViewById(R.id.rgPilihanJawaban);
        rbOpsiA = findViewById(R.id.rbPilihanJawaban1);
        rbOpsiB = findViewById(R.id.rbPilihanJawaban2);
        rbOpsiC = findViewById(R.id.rbPilihanJawaban3);
        rbOpsiD = findViewById(R.id.rbPilihanJawaban4);
        btnSubmitPretest = findViewById(R.id.btnSubmitPretest);
        btnSubmitPretest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cekJawaban();
            }
        });
        showDialogPretest();
        loadPretest();
    }


    private void cekJawaban() {
        if (rbOpsiA.isChecked()) {
            if (rbOpsiA.getText().toString().equals(jawaban)) {
                skor = skor + 10;
                //Utils.toastSuccess(getApplicationContext(),"BENAR");
                setContent();
            } else {
                //Utils.toastAlert(getApplicationContext(), "SALAH");
                setContent();
            }
        } else if (rbOpsiB.isChecked()) {
            if (rbOpsiB.getText().toString().equals(jawaban)) {
                skor = skor + 10;
                //Utils.toastSuccess(getApplicationContext(),"BENAR");
                setContent();
            } else {
                //Utils.toastAlert(getApplicationContext(), "SALAH");
                setContent();
            }
        } else if (rbOpsiC.isChecked()) {
            if (rbOpsiC.getText().toString().equals(jawaban)) {
                skor = skor + 10;
                //Utils.toastSuccess(getApplicationContext(),"BENAR");
                setContent();
            } else {
                //Utils.toastAlert(getApplicationContext(), "SALAH");
                setContent();
            }
        } else if (rbOpsiD.isChecked()) {
            if (rbOpsiD.getText().toString().equals(jawaban)) {
                skor = skor + 10;
                //Utils.toastSuccess(getApplicationContext(),"BENAR");
                setContent();
            } else {
                //Utils.toastAlert(getApplicationContext(), "SALAH");
                setContent();
            }
        } else {
            Utils.toastAlert(getApplicationContext(), "Tentukan jawaban anda");
        }
    }

    private void setContent() {
        System.out.println("Banyak Soal = " + list_soal.size());
        System.out.println("SCORE MENTAH = " + skor);
        arrSoal = list_soal.size();
        if (arrSoal == 0) {
            Utils.toastAlert(this, "SOAL PRETEST KOSONG!");
            Intent i = new Intent(getApplicationContext(), DetailMateriActivity.class);
            i.putExtra(EXTRA_ID_MATERI, idMateri);
            i.putExtra(EXTRA_TYPE_MATERI, typeMateri);
            startActivity(i);
        } else {
            double hasil = skor * 10 / arrSoal;
            jumlahSkor = String.valueOf(Math.round(hasil));

            if (x >= arrSoal) {
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                stop = sdf.format(c);
                String[] splt = start.split(" ");
                tglSelesai = splt[0];
                jamSelesai = splt[1];
                Log.e("TANGGAL SELESAI", tglSelesai);
                Log.e("JAM SELESAI ", jamSelesai);
                Log.e("STOP ", stop);

                postNilaiPretest();

                Intent i = new Intent(getApplicationContext(), DetailMateriActivity.class);
                i.putExtra(EXTRA_ID_MATERI, idMateri);
                i.putExtra(EXTRA_TYPE_MATERI, typeMateri);
                startActivity(i);
                System.out.println("SCORE = " + jumlahSkor);
            } else {
                spm = list_soal.get(x);
                txtSoal.setText(spm.getSoal());
                rbOpsiA.setText(spm.getOpsiA());
                rbOpsiB.setText(spm.getOpsiB());
                rbOpsiC.setText(spm.getOpsiC());
                rbOpsiD.setText(spm.getOpsiD());
                jawaban = spm.getKunciJawaban();
                radioGroup.clearCheck();

            }
            x++;
            radioGroup.clearCheck();
        }
    }


    private void uncheckedRadioButton() {
        rbOpsiA.setChecked(false);
        rbOpsiB.setChecked(false);
        rbOpsiC.setChecked(false);
        rbOpsiD.setChecked(false);
        rbOpsiA.clearFocus();
        rbOpsiB.clearFocus();
        rbOpsiC.clearFocus();
        rbOpsiD.clearFocus();
    }

    private void showDialogPretest() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_pretest);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void loadPretest() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        start = sdf.format(c);
        String[] splt = start.split(" ");
        tglMulai = splt[0];
        jamMulai = splt[1];
        Log.e("TANGGAL MULAI", tglMulai);
        Log.e("JAM MULAI ", jamMulai);
        Log.e("START ", start);

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        System.out.println("TOKEN = " + token);

        try {
            JSONObject obj = new JSONObject();
            obj.put("materi", idMateri);
            obj.put("type", "pretest");

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "getsoaltes",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");

                            System.out.println("message = " + message);

                            String jArray = jresp.getString("data");
                            JSONArray array = new JSONArray(jArray);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject data = array.getJSONObject(i);
                                String soalPretest = data.getString("pertanyaan");
                                String opsiA = data.getString("opsi_a");
                                String opsiB = data.getString("opsi_b");
                                String opsiC = data.getString("opsi_c");
                                String opsiD = data.getString("opsi_d");
                                String opsiE = data.getString("opsi_e");
                                String kunciJawaban = data.getString("kunci_jawaban");

                                System.out.println("soalPretest = " + soalPretest);

                                spm = new SoalPretestModel();
                                spm.setSoal(soalPretest);
                                spm.setOpsiA(opsiA);
                                spm.setOpsiB(opsiB);
                                spm.setOpsiC(opsiC);
                                spm.setOpsiD(opsiD);
                                spm.setOpsiE(opsiE);
                                spm.setKunciJawaban(kunciJawaban);
                                list_soal.add(spm);
                            }

                            adapter = new ArrayAdapter<>(PretestActivity.this, R.layout.support_simple_spinner_dropdown_item, list_soal);
                            adapter.notifyDataSetChanged();
                            setContent();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(PretestActivity.this).add(stringRequest);
    }

    private void postNilaiPretest() {
        hitungWaktu();
//        Log.e("TANGGAL MULAI2", tglMulai);
//        Log.e("JAM MULAI2 ", jamMulai);
//        Log.e("TANGGAL SELESAI_2", tglSelesai);
//        Log.e("JAM SELESAI_2", jamSelesai);
//        Log.e("SCORE_PRETEST", jumlahSkor);
//        Log.e("LAMA_WAKTU", lamaWaktu);


        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        String idUser = user.get(SessionManager.KEY_ID_USER);
        System.out.println("TOKEN = " + token);

        try {
            JSONObject obj = new JSONObject();
            obj.put("id_materi", idMateri);
            obj.put("type_test", "pretest");
            obj.put("id_user", idUser);
            obj.put("nilai_test", jumlahSkor);
            obj.put("tgl_mulai", tglMulai);
            obj.put("jam_mulai", jamMulai);
            obj.put("tgl_selesai", tglSelesai);
            obj.put("jam_selesai", jamSelesai);
            obj.put("jml_wkt_finish", lamaWaktu);
            obj.put("komplain", "");

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "posnilai",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");

                            System.out.println("message = " + message);
                            if (message.equals("Success")) {
                                Log.e("NILAIPRETEST", "Berhasil di kirim");
                            } else {
                                Log.e("NILAIPRETEST", "GAGAL di kirim");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(PretestActivity.this).add(stringRequest);
    }

    private void hitungWaktu() {

        SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        try {
            d1 = dFormat.parse(start);
            d2 = dFormat.parse(stop);
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");

            lamaWaktu = String.valueOf(String.format("%02d", diffHours) + ":" + String.format("%02d", diffMinutes));
            Log.e("LAMA_WAKTU", String.valueOf(String.format("%02d", diffHours) + ":" + String.format("%02d", diffMinutes)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
//
//                dialog.setContentText("Apakah anda yakin untuk keluar dari halaman Pretest?")
//                .setConfirmText("Iya")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        dialog.dismissWithAnimation();
//                        startActivity(new Intent(PretestActivity.this, MateriActivity.class));
//                    }
//                });
//                dialog.show();

        startActivity(new Intent(PretestActivity.this, HomeActivity.class));
    }
}