package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.HashMap;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView iv = findViewById(R.id.image);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.splashtransition);
        iv.startAnimation(animation);

        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                    cekStatusLogin();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        timer.start();
    }

    private void cekStatusLogin() {
        SessionManager sessionManager = new SessionManager(getApplicationContext());

        sessionManager.checkLogin();
        HashMap<String, String> user = sessionManager.getUserDetails();
        String nik = user.get(SessionManager.KEY_NIK);
        String token = user.get(SessionManager.KEY_TOKEN);

        System.out.println("TOKEN = "+token);
        System.out.println("NIK = "+nik);

        final Intent i = new Intent(this, LoginActivity.class);

        if (nik == null || token == null){
            startActivity(i);
        } else {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        }
    }


}