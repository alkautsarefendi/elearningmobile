package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.adapter.MateriAdapter;
import test.alkautsar.elearningmobile.model.DataMateri;
import test.alkautsar.elearningmobile.utils.SpacingItemDecoration;
import test.alkautsar.elearningmobile.utils.Tools;
import test.alkautsar.elearningmobile.utils.Utils;

public class MateriActivity extends AppCompatActivity {

    public static final String EXTRA_ID_MATERI = "idMateri";
    public static final String EXTRA_JUDUL = "judul";
    public static final String EXTRA_TYPE_MATERI = "typeMateri";
    SwipeRefreshLayout swipeRefreshLayout;
    private List<DataMateri> data_materi;
    String json;
    RecyclerView rvMateri;
    String typeMateri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);

        rvMateri = findViewById(R.id.rv_materi_training);

        data_materi = new ArrayList<>();

        rvMateri.setLayoutManager(new GridLayoutManager(this, 2));
        rvMateri.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 10), true));
        rvMateri.setHasFixedSize(true);

        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh
                swipeRefreshLayout.setRefreshing(false);
                loadMateri();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadMateri();
    }

    private void loadMateri() {

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        System.out.println("TOKEN = "+token);

        try{
            JSONObject obj = new JSONObject();
            obj.put("divisi","1");
            obj.put("type","Training");

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e){
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header)+"materi",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        data_materi.clear();
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            String path = jresp.getString("path");

                            System.out.println("message = " + message);
                            System.out.println("path = " + path);

                            String jArray = jresp.getString("data");
                            JSONArray array = new JSONArray(jArray);
                            for (int i = 0; i < array.length(); i++){
                                JSONObject data = array.getJSONObject(i);
                                String idMateri = data.getString("id");
                                String namaMateri = data.getString("nama");
                                typeMateri = data.getString("type");
                                String idDivisi = data.getString("id_divisi");
                                String image = data.getString("image");
                                String insertID = data.getString("insert_id");
                                String insertDate = data.getString("insert_date");
                                String status = data.getString("status");

                                System.out.println("idMateri = " + idMateri);
                                System.out.println("namaMateri = " + namaMateri);
                                System.out.println("type = " + typeMateri);
                                System.out.println("idDivisi = " + idDivisi);
                                System.out.println("image = " + image);
                                System.out.println("insertID = " + insertID);
                                System.out.println("insertDate = " + insertDate);
                                System.out.println("status = " + status);

                                DataMateri dm = new DataMateri();
                                dm.setId_materi(idMateri);
                                dm.setNama(namaMateri);
                                dm.setType(typeMateri);
                                dm.setId_divisi(idDivisi);
                                dm.setImage(image);
                                dm.setInsert_id(insertID);
                                dm.setInsert_date(insertDate);
                                dm.setStatus(status);
                                dm.setPath(path);
                                data_materi.add(dm);
                            }

                            MateriAdapter adapter = new MateriAdapter(MateriActivity.this, data_materi);
                            adapter.notifyDataSetChanged();
                            rvMateri.setAdapter(adapter);
                            adapter.setOnItemClickListener(new MateriAdapter.OnItemClickListener() {
                                @Override
                                public void onMateriClick(int position) {
                                    Intent detailIntent = new Intent(getApplicationContext(), PretestActivity.class);
                                    DataMateri clickedItem = data_materi.get(position);

                                    detailIntent.putExtra(EXTRA_ID_MATERI, clickedItem.getId_materi());
                                    System.out.println("ID = " + clickedItem.getId_materi());
                                    detailIntent.putExtra(EXTRA_JUDUL, clickedItem.getNama());
                                    detailIntent.putExtra(EXTRA_TYPE_MATERI, clickedItem.getType());

                                    startActivity(detailIntent);
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(),"Gagal Mengambil Data!"+e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(),"Gagal Mengambil Data!");
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  "+token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee){
                    return null;
                }
            }
        };



        //adding our stringrequest to queue
        Volley.newRequestQueue(MateriActivity.this).add(stringRequest);

    }

    public void goBack(View view) {
        onBackPressed();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
//
//                dialog.setContentText("Apakah anda yakin untuk keluar dari halaman Pretest?")
//                .setConfirmText("Iya")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        dialog.dismissWithAnimation();
//                        startActivity(new Intent(PretestActivity.this, MateriActivity.class));
//                    }
//                });
//                dialog.show();

        startActivity(new Intent(MateriActivity.this, HomeActivity.class));
    }
}