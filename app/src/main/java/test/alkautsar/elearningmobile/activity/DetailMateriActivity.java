package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.model.DataMateri;
import test.alkautsar.elearningmobile.model.ImageMateriModel;
import test.alkautsar.elearningmobile.model.SoalPretestModel;
import test.alkautsar.elearningmobile.utils.Utils;

public class DetailMateriActivity extends AppCompatActivity {

    public static final String EXTRA_ID_MATERI = "idMateri";
    public static final String EXTRA_TYPE_MATERI = "typeMateri";
    ImageView imgMateriDetail;
    TextView tv_materiDetail;
    Button btnNextMateriDetail;
    private String idMateri, json;
    private List<ImageMateriModel> img_materi;
    private ArrayAdapter adapter;
    private int x, arrImg;
    ImageMateriModel im ;
    String dateStart, dateStop, typeMateri;
    Date d1 = null;
    Date d2 = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_materi);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        img_materi = new ArrayList<>();

        Intent intent = getIntent();
        idMateri = intent.getStringExtra(EXTRA_ID_MATERI);
        typeMateri = intent.getStringExtra(EXTRA_TYPE_MATERI);
        System.out.println("idMateri = " + idMateri);
        System.out.println("typeMateri = " + typeMateri);

        imgMateriDetail = findViewById(R.id.imgMateriDetail);
        tv_materiDetail = findViewById(R.id.txtHeaderDetailMateri);
        btnNextMateriDetail = findViewById(R.id.btnNextMateriDetail);
        btnNextMateriDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setImgMateri();
            }
        });

        loadMateriDetail();

    }
    private void setImgMateri() {
        System.out.println("Banyak MATERI = " + img_materi.size());
        arrImg = img_materi.size();

        if (x >= arrImg) {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            dateStop = dFormat.format(c);
            Log.e("WAKTU_SELESAI", dateStop);
            hitungwaktu();
            Intent i = new Intent(getApplicationContext(), DetailMateriVideoActivity.class);
            i.putExtra(EXTRA_ID_MATERI,idMateri);
            i.putExtra(EXTRA_TYPE_MATERI, typeMateri);
            startActivity(i);
        } else {

            im = img_materi.get(x);
            System.out.println("KANCIANG = "+im.getPath()+"/"+idMateri+"/"+im.getImgMateri());

            Glide.with(this)
                    .load(im.getPath()+"/"+idMateri+"/"+im.getImgMateri())
                    .into(imgMateriDetail);


        }
        x++;
        overridePendingTransition(R.anim.slide_in,  R.anim.slide_out);
    }

    @SuppressLint("DefaultLocale")
    private void hitungwaktu() {
        SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        try {
            d1 = dFormat.parse(dateStart);
            d2 = dFormat.parse(dateStop);
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            Log.e("LAMA_WAKTU", String.valueOf(String.format("%02d",diffHours)+":"+String.format("%02d",diffMinutes)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadMateriDetail() {

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        dateStart = dFormat.format(c);
        Log.e("WAKTU_MULAI", dateStart);

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        System.out.println("TOKEN = " + token);

        try {
            JSONObject obj = new JSONObject();
            obj.put("id", idMateri);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "materi/detail",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            System.out.println("message = " + message);
                            String path = jresp.getString("path");
                            String data = jresp.getString("data");
                            JSONObject dataObj = new JSONObject(data);
                            String imgList = dataObj.getString("images");
                            String list = dataObj.getString("list");

                            JSONArray imgArray = new JSONArray(imgList);
                            for (int i = 0; i < imgArray.length(); i++){
                                JSONObject imgJSON = imgArray.getJSONObject(i);
                                String fileImage = imgJSON.getString("image");

                                im = new ImageMateriModel();
                                im.setImgMateri(fileImage);
                                im.setPath(path);
                                img_materi.add(im);
                            }
                            adapter = new ArrayAdapter<>(DetailMateriActivity.this,R.layout.support_simple_spinner_dropdown_item,img_materi);
                            adapter.notifyDataSetChanged();
                            setImgMateri();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(DetailMateriActivity.this).add(stringRequest);
    }


}