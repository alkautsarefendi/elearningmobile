package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.HashMap;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;

public class ProfileActivity extends AppCompatActivity {

    TextView namaUser1, namaUser2, emailUser1, emailUser2, nikUser, kantor, divisi, jabatan, tglLahir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        namaUser1 = findViewById(R.id.txtNamaUser1);
        namaUser2 = findViewById(R.id.txtnamaUser2);
        emailUser1 = findViewById(R.id.txtEmailUser);
        emailUser2 = findViewById(R.id.txtEmailUser2);
        nikUser = findViewById(R.id.txtNikUser);
        kantor = findViewById(R.id.txtKantor);
        divisi = findViewById(R.id.txtDivisi);
        jabatan = findViewById(R.id.txtjabatan);
        tglLahir = findViewById(R.id.txtTanggalLahir);

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String namaUser = user.get(SessionManager.KEY_NAME);
        String emailUser = user.get(SessionManager.KEY_EMAIL);
        String nikUserProfile = user.get(SessionManager.KEY_NIK);
        String kantorUser = user.get(SessionManager.KEY_CABANG);
        String divisiUser = user.get(SessionManager.KEY_DIVISI);
        String jabatanUser = user.get(SessionManager.KEY_JABATAN);
        String tglLahirUser = user.get(SessionManager.KEY_TGL_LAHIR);

        namaUser1.setText(namaUser);
        namaUser2.setText(namaUser);
        emailUser1.setText(emailUser);
        emailUser2.setText(emailUser);
        nikUser.setText(nikUserProfile);
        kantor.setText(kantorUser);
        divisi.setText(divisiUser);
        jabatan.setText(jabatanUser);
        tglLahir.setText(tglLahirUser);

    }
}