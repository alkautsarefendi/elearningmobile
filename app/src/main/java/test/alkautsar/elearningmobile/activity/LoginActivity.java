package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.alkautsar.elearningmobile.API.APIService;
import test.alkautsar.elearningmobile.API.LoginModel;
import test.alkautsar.elearningmobile.API.Result;
import test.alkautsar.elearningmobile.API.RetrofitClient;
import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.adapter.EbookAdapter;
import test.alkautsar.elearningmobile.model.DataEbook;
import test.alkautsar.elearningmobile.model.ImageMateriModel;
import test.alkautsar.elearningmobile.utils.Utils;

public class LoginActivity extends AppCompatActivity {

    SessionManager session;

    private SignInButton signInButton;
    private ImageButton imgGoogle;
    private GoogleSignInClient googleSignInClient;
    private String TAG = "MainActivity";
    private FirebaseAuth mAuth;
    private String idGoogle;
    private int RC_SIGN_IN = 1;
    private TextView daftar, lupaPass;
    private Button btnLogin;
    private EditText edtNikUser, edtPassword;
    private String json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new SessionManager(getApplicationContext());

        lupaPass = findViewById(R.id.lupaPassword);
        edtNikUser = findViewById(R.id.txtNikUser);
        edtPassword = findViewById(R.id.txtPassword);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nik = edtNikUser.getText().toString();
                String pass = edtPassword.getText().toString();
                if (nik == null || pass == null) {
                    Utils.toastAlert(LoginActivity.this, "NIK dan Password tidak boleh kosong");
                } else if (nik.trim().length() < 9) {
                    Utils.toastAlert(LoginActivity.this, "NIK tidak valid");
                } else {
                    postLoginUser();
                }

            }
        });

        lupaPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
                dialog.setContentView(R.layout.dialog_lupa_password);
                dialog.setCancelable(true);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


                ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

                dialog.show();
                dialog.getWindow().setAttributes(lp);
            }
        });
        daftar = findViewById(R.id.lnkRegistrasi);
        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterUserActivity.class));
            }
        });

    }

    private void postLoginUser() {

        final Dialog dialog = new Dialog(LoginActivity.this, R.style.mytheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_progres);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

        String nik = edtNikUser.getText().toString();
        String pass = edtPassword.getText().toString();

        try {
            JSONObject obj = new JSONObject();
            obj.put("email", nik);
            obj.put("password", pass);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "login",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            String token_user = jresp.getString("token");
                            if (message.equals("Success")) {
                                String jArray = jresp.getString("data");
                                JSONArray array = new JSONArray(jArray);
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject data = array.getJSONObject(i);
                                    String idUser = data.getString("id");
                                    String namaUser = data.getString("name");
                                    String nik_user = data.getString("nik");
                                    String kantor = data.getString("nm_cabang");
                                    String divisi = data.getString("nm_divisi");
                                    String jabatan = data.getString("nm_jabatan");
                                    String tgl_lahir = data.getString("tgl_lahir");
                                    String email_user = data.getString("email_user");


                                    SessionManager sessionManager = new SessionManager(getApplicationContext());
                                    sessionManager.createLoginSession(nik_user, token_user);
                                    sessionManager.createDetailUser(namaUser, email_user, kantor,
                                            divisi, jabatan, tgl_lahir, nik_user, idUser);
                                    Utils.toastSuccess(LoginActivity.this, "Login Sukses");
                                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                    dialog.dismiss();
                                    ;
                                }

                            } else {
                                Utils.toastAlert(LoginActivity.this, "Login Gagal!");
                                dialog.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(LoginActivity.this, "Login Gagal!");
                            dialog.dismiss();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(LoginActivity.this, "Login Gagal!");
                        dialog.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };


        //adding our stringrequest to queue
        Volley.newRequestQueue(LoginActivity.this).add(stringRequest);
    }

//    private void loginUser() {
//        final Dialog dialog = new Dialog(LoginActivity.this, R.style.mytheme);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.loading_progres);
//        dialog.setCanceledOnTouchOutside(true);
//        dialog.show();
//
//        String nik = edtNikUser.getText().toString();
//        String pass = edtPassword.getText().toString();
//
//        APIService service = RetrofitClient.getClient().create(APIService.class);
//
//        LoginModel loginModel = new LoginModel();
//        loginModel.setNik(nik);
//        loginModel.setPassword(pass);
//
//        Call<Result> call = service.login(loginModel.getNik(), loginModel.getPassword());
//        System.out.println("INPUT = "+loginModel.getNik()+","+ loginModel.getPassword());
//        call.enqueue(new Callback<Result>() {
//            @Override
//            public void onResponse(@NotNull Call<Result> call, @NotNull Response<Result> response) {
//                Result result = response.body();
//                if (response.isSuccessful()){
//                    String token = result.getResponse().getToken();
//                    String nik = edtNikUser.getText().toString();
//                    System.out.println("TOKEN = "+result.getResponse().getToken());
//
//                    SessionManager sessionManager = new SessionManager(getApplicationContext());
//                    sessionManager.createLoginSession(nik, token);
//
//                    Utils.toastSuccess(LoginActivity.this, "Login Sukses");
//                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//                    dialog.dismiss();;
//                } else {
//                    Utils.toastAlert(LoginActivity.this, "Login Gagal!");
//                    dialog.dismiss();;
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Result> call, Throwable t) {
//                Utils.toastAlert(LoginActivity.this, "Login gagal!");
//                dialog.dismiss();;
//            }
//        });
//    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
        alertDialog.setTitle("BPR KREDIT MANDIRI");
        alertDialog.setIcon(R.drawable.logobpr);
        alertDialog.setMessage("Keluar dari Aplikasi?");
        alertDialog.setIcon(R.drawable.logobpr);
        alertDialog.setPositiveButton("IYA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                LoginActivity.super.onBackPressed();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
                startActivity(intent);
                finish();
                System.exit(0);
            }
        });

        alertDialog.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).create().show();

    }


}