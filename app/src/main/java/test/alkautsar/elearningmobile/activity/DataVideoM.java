package test.alkautsar.elearningmobile.activity;

public class DataVideoM {

    String id, judulVideoM, file, path, desk;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudulVideoM() {
        return judulVideoM;
    }

    public void setJudulVideoM(String judulVideoM) {
        this.judulVideoM = judulVideoM;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDesk() {
        return desk;
    }

    public void setDesk(String desk) {
        this.desk = desk;
    }
}
