package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.adapter.MateriAdapter;
import test.alkautsar.elearningmobile.adapter.VideoAdapter;
import test.alkautsar.elearningmobile.utils.SpacingItemDecoration;
import test.alkautsar.elearningmobile.utils.Tools;
import test.alkautsar.elearningmobile.utils.Utils;

public class MateriVidioActivity extends AppCompatActivity {

    RecyclerView rvVideoMotivasi;
    private List<DataVideoM> data_video_motivasi;
    SwipeRefreshLayout swipeRefreshLayout;
    String json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi_vidio);

        rvVideoMotivasi = findViewById(R.id.rv_video_motivasi);

        data_video_motivasi = new ArrayList<>();

        rvVideoMotivasi.setLayoutManager(new GridLayoutManager(this, 2));
        rvVideoMotivasi.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 10), true));
        rvVideoMotivasi.setHasFixedSize(true);

        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh
                swipeRefreshLayout.setRefreshing(false);
                loadVideo();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadVideo();
    }

    private void loadVideo() {

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        System.out.println("TOKEN = " + token);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "video/motivasi",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        data_video_motivasi.clear();
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            String path = jresp.getString("path");

                            if (message.equals("Success")) {
                                String jArray = jresp.getString("data");
                                JSONArray array = new JSONArray(jArray);
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject data = array.getJSONObject(i);
                                    String idVideoM = data.getString("id");
                                    String judulVideo = data.getString("nama");
                                    String file = data.getString("file");
                                    String desc = data.getString("deskripsi");

                                    DataVideoM dv = new DataVideoM();
                                    dv.setId(idVideoM);
                                    dv.setJudulVideoM(judulVideo);
                                    dv.setFile(file);
                                    dv.setPath(path);
                                    dv.setDesk(desc);
                                    data_video_motivasi.add(dv);
                                }
                            }

                            VideoAdapter adapter = new VideoAdapter(MateriVidioActivity.this, data_video_motivasi);
                            adapter.notifyDataSetChanged();
                            rvVideoMotivasi.setAdapter(adapter);
                            adapter.setOnItemClickListener(new MateriAdapter.OnItemClickListener() {
                                @Override
                                public void onMateriClick(int position) {
                                    Intent detailIntent = new Intent(getApplicationContext(), DetailVideoMSActivity.class);
                                    DataVideoM clickedItem = data_video_motivasi.get(position);
                                    String url_video = clickedItem.getPath()+clickedItem.getFile();
                                    String judulVideo = clickedItem.getJudulVideoM();
                                    String deskripsi = clickedItem.getDesk();

                                    detailIntent.putExtra("URL_VIDEO", url_video);
                                    detailIntent.putExtra("JUDUL_VIDEO", judulVideo);
                                    detailIntent.putExtra("DESK_VIDEO", deskripsi);
                                    System.out.println("URL_VIDEO = " + url_video);
                                    System.out.println("JUDUL_VIDEO = " + judulVideo);
                                    System.out.println("DESK_VIDEO = " + deskripsi);
                                    startActivity(detailIntent);
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };


        //adding our stringrequest to queue
        Volley.newRequestQueue(MateriVidioActivity.this).add(stringRequest);

    }


    public void goBack(View view) {
        onBackPressed();
    }
}