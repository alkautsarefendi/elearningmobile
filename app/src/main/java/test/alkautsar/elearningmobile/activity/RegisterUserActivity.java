package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import test.alkautsar.elearningmobile.API.APIService;
import test.alkautsar.elearningmobile.API.APIUrl;
import test.alkautsar.elearningmobile.API.Result;
import test.alkautsar.elearningmobile.API.RegisterModel;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.model.SpinnerCabangModel;
import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.utils.Utils;
import test.alkautsar.elearningmobile.model.SpinnerDivisiModel;
import test.alkautsar.elearningmobile.model.SpinnerJabatanModel;

public class RegisterUserActivity extends AppCompatActivity {

    private SimpleDateFormat dateFormatter;
    private EditText edtNIK, edtNama, edtTanggalLahir, edtPass, edtEmail, edtKonfPass;
    private Spinner spinCabang, spinJabatan, spinDivisi;
    private CheckBox cbConfirm;
    private String json;
    TextView txtIdCabang, txtIdDivisi, txtIdJabatan;
    String nik, nama, pass, email, tglLahir, cbg, dvs, jbt;

    ArrayList<String> cabList, divList, jabList;
    ArrayList<SpinnerCabangModel> listCab;
    ArrayList<SpinnerDivisiModel> listDiv;
    ArrayList<SpinnerJabatanModel> listJab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        edtNIK = findViewById(R.id.txtNik);
        edtNama = findViewById(R.id.txtNama);
        edtPass = findViewById(R.id.txtPassUser);
        edtEmail = findViewById(R.id.txtEmailUser);
        edtKonfPass = findViewById(R.id.txtKonfirmPass);
        edtTanggalLahir = findViewById(R.id.txtTanggalLahir);
        spinCabang = findViewById(R.id.spinCabang);
        spinJabatan = findViewById(R.id.spinJabatan);
        spinDivisi = findViewById(R.id.spinDivisi);
        cbConfirm = findViewById(R.id.cbConfirm);
        txtIdCabang = findViewById(R.id.txtIdCabang);
        txtIdDivisi = findViewById(R.id.txtIdDivisi);
        txtIdJabatan = findViewById(R.id.txtIdJabatan);

        loadDataCabang();
        Button btnSubmitRegister = findViewById(R.id.btnSubmitRegister);
        btnSubmitRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        edtTanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        spinCabang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e("CABANG ID =", listCab.get(position).getIdCabang());
                Log.e("CABANG POSITION =", String.valueOf(listCab.get(position)));
                String idCab = listCab.get(position).getIdCabang();
                txtIdCabang.setText(idCab);
                if (position != 0) {
                    loadDataDivisi();
                } else {
                    Log.e("TAG", "BELUM DIPILIH");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinDivisi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e("DIVISI ID =", listDiv.get(position).getIdDivisi());
                String idDiv = listDiv.get(position).getIdDivisi();
                txtIdDivisi.setText(idDiv);
                if (position != 0) {
                    loadDataJabatan();
                } else {
                    Log.e("TAG", "BELUM DIPILIH");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinJabatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e("JABATAN ID =", listJab.get(position).getIdJabatan());
                String idDiv = listJab.get(position).getIdJabatan();
                txtIdJabatan.setText(idDiv);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void submit() {

        final Dialog dialog = new Dialog(RegisterUserActivity.this, R.style.mytheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_progres);
        dialog.setCanceledOnTouchOutside(true);

        nik = edtNIK.getText().toString();
        nama = edtNama.getText().toString();
        pass = edtPass.getText().toString();
        email = edtEmail.getText().toString();
        tglLahir = edtTanggalLahir.getText().toString();


        if (nik.isEmpty()) {
            Utils.toastAlert(RegisterUserActivity.this, "NIK tidak boleh kosong");
        } else if (!edtKonfPass.getText().toString().equals(pass)) {
            Utils.toastAlert(RegisterUserActivity.this, "Password tidak sama");
        } else if (nik.length() < 9) {
            Utils.toastAlert(RegisterUserActivity.this, "Nik tidak lengkap");
        } else if (nama.isEmpty()) {
            Utils.toastAlert(RegisterUserActivity.this, "Nama tidak boleh kosong");
        } else if (pass.isEmpty()) {
            Utils.toastAlert(RegisterUserActivity.this, "Password tidak boleh kosong");
        } else if (email.isEmpty()) {
            Utils.toastAlert(RegisterUserActivity.this, "Email tidak boleh kosong");
        } else if (tglLahir.isEmpty()) {
            Utils.toastAlert(RegisterUserActivity.this, "Tanggal Lahir tidak boleh kosong");
        } else if (spinCabang.getSelectedItemId() == 0) {
            Utils.toastAlert(RegisterUserActivity.this, "Kantor tidak boleh kosong");
        } else if (spinDivisi.getSelectedItemId() == 0) {
            Utils.toastAlert(RegisterUserActivity.this, "Divisi tidak boleh kosong");
        } else if (spinJabatan.getSelectedItemId() == 0) {
            Utils.toastAlert(RegisterUserActivity.this, "Jabatan tidak boleh kosong");
        } else if (!cbConfirm.isChecked()) {
            Utils.toastAlert(RegisterUserActivity.this, "Agree Term of Use Please");
        } else {

            dialog.show();
            String idKantor = txtIdCabang.getText().toString();
            String idDivisi = txtIdDivisi.getText().toString();
            String idJabatan = txtIdJabatan.getText().toString();

            try {
                JSONObject obj = new JSONObject();
                obj.put("name", nama);
                obj.put("email_user", email);
                obj.put("password", pass);
                obj.put("nik", nik);
                obj.put("divisi", idDivisi);
                obj.put("jabatan", idJabatan);
                obj.put("cabang", idKantor);
                obj.put("level", "2");
                obj.put("tgl_lahir", tglLahir);
                obj.put("status", "1");

                json = obj.toString(2);
                Log.d("output", obj.toString(2));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "register",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        //converting the string to json array object

                                        Log.d("strrrrr", ">>" + response);

                                        JSONObject jObj = new JSONObject(response);
                                        String respon = jObj.getString("response");
                                        JSONObject jresp = new JSONObject(respon);
                                        String respToken = jresp.getString("token");
                                        String respMessage = jresp.getString("message");
                                        dialog.dismiss();
                                        if (respMessage.equals("Success")) {
                                            Utils.toastSuccess(RegisterUserActivity.this, "Pendaftaran Berhasil");
                                            startActivity(new Intent(RegisterUserActivity.this, LoginActivity.class));
                                        } else {
                                            dialog.dismiss();
                                            Utils.toastAlert(RegisterUserActivity.this, "Pendaftaran Gagal");
                                        }
                                    } catch (JSONException e) {
                                        dialog.dismiss();
                                        Utils.toastAlert(RegisterUserActivity.this, "Pendaftaran Gagal");
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    dialog.dismiss();
                                    Log.e("ERROR", error.toString());
                                    Utils.toastAlert(RegisterUserActivity.this, "Pendaftaran Gagal");
                                }
                            }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            return headers;
                        }

                        @Override
                        public byte[] getBody() throws AuthFailureError {

                            try {
                                return json == null ? null : json.getBytes("utf-8");

                            } catch (UnsupportedEncodingException uee) {
                                return null;
                            }
                        }
                    };

                    //adding our stringrequest to queue
                    Volley.newRequestQueue(RegisterUserActivity.this).add(stringRequest);
                }
            });
        }
    }

    private void loadDataCabang() {

        cabList = new ArrayList<>();
        listCab = new ArrayList<>();

        final ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, cabList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                StringRequest stringRequest = new StringRequest(Request.Method.GET, getString(R.string.url_header) + "cabang",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    //converting the string to json array object

                                    Log.d("strrrrr", ">>" + response);
                                    cabList.clear();

                                    JSONObject jObj = new JSONObject(response);
                                    String respon = jObj.getString("response");
                                    JSONObject jresp = new JSONObject(respon);
                                    String jArray = jresp.getString("data");
                                    JSONArray array = new JSONArray(jArray);
                                    //traversing through all the object
                                    for (int i = 0; i < array.length(); i++) {

                                        //getting product object from json array
                                        JSONObject product = array.getJSONObject(i);
                                        String id = product.getString("id");
                                        String namaCabang = product.getString("nama");

                                        System.out.println("ID = " + id);
                                        System.out.println("CABANG = " + namaCabang);

                                        SpinnerCabangModel sm = new SpinnerCabangModel();
                                        sm.setIdCabang(id);
                                        sm.setNamaCabang(namaCabang);
                                        listCab.add(sm);
                                        cabList.add(namaCabang);

                                    }
                                    spinCabang.setAdapter(spinnerArrayAdapter1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Utils.toastAlert(getApplicationContext(), error.toString());
                            }
                        });

                //adding our stringrequest to queue
                Volley.newRequestQueue(RegisterUserActivity.this).add(stringRequest);
            }
        });
    }

    private void loadDataDivisi() {

        divList = new ArrayList<>();
        listDiv = new ArrayList<>();

        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, divList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        String idCab = txtIdCabang.getText().toString();

        try {
            JSONObject obj = new JSONObject();
            obj.put("cabang", idCab);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "divisi",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    //converting the string to json array object

                                    Log.d("strrrrr", ">>" + response);

                                    divList.clear();

                                    JSONObject jObj = new JSONObject(response);
                                    String respon = jObj.getString("response");
                                    JSONObject jresp = new JSONObject(respon);
                                    String jArray = jresp.getString("data");
                                    JSONArray array = new JSONArray(jArray);
                                    //traversing through all the object
                                    for (int i = 0; i < array.length(); i++) {

                                        //getting product object from json array
                                        JSONObject product = array.getJSONObject(i);
                                        String id = product.getString("id");
                                        String namaDivisi = product.getString("nama");

                                        System.out.println("ID = " + id);
                                        System.out.println("DIVISI = " + namaDivisi);

                                        SpinnerDivisiModel sdm = new SpinnerDivisiModel();
                                        sdm.setIdDivisi(id);
                                        sdm.setNamaDivisi(namaDivisi);
                                        listDiv.add(sdm);
                                        divList.add(namaDivisi);
                                    }
                                    spinDivisi.setAdapter(spinnerArrayAdapter2);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Utils.toastAlert(getApplicationContext(), error.toString());
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {

                        try {
                            return json == null ? null : json.getBytes("utf-8");

                        } catch (UnsupportedEncodingException uee) {
                            return null;
                        }
                    }
                };


                //adding our stringrequest to queue
                Volley.newRequestQueue(RegisterUserActivity.this).add(stringRequest);
            }
        });
    }

    private void loadDataJabatan() {

        jabList = new ArrayList<>();
        listJab = new ArrayList<>();

        final ArrayAdapter<String> spinnerArrayAdapter3 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, jabList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        String idCab = txtIdCabang.getText().toString();
        String idDiv = txtIdDivisi.getText().toString();

        try {
            JSONObject obj = new JSONObject();
            obj.put("cabang", idCab);
            obj.put("divisi", idDiv);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "jabatan",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    //converting the string to json array object

                                    Log.d("strrrrr", ">>" + response);

                                    divList.clear();

                                    JSONObject jObj = new JSONObject(response);
                                    String respon = jObj.getString("response");
                                    JSONObject jresp = new JSONObject(respon);
                                    String jArray = jresp.getString("data");
                                    JSONArray array = new JSONArray(jArray);
                                    //traversing through all the object
                                    for (int i = 0; i < array.length(); i++) {

                                        //getting product object from json array
                                        JSONObject product = array.getJSONObject(i);
                                        String id = product.getString("id");
                                        String namaJabatan = product.getString("nama");

                                        System.out.println("ID = " + id);
                                        System.out.println("JABATAN = " + namaJabatan);

                                        SpinnerJabatanModel sj = new SpinnerJabatanModel();
                                        sj.setIdJabatan(id);
                                        sj.setNamaJabatan(namaJabatan);
                                        listJab.add(sj);
                                        jabList.add(namaJabatan);
                                    }
                                    spinJabatan.setAdapter(spinnerArrayAdapter3);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Utils.toastAlert(getApplicationContext(), error.toString());
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {

                        try {
                            return json == null ? null : json.getBytes("utf-8");

                        } catch (UnsupportedEncodingException uee) {
                            return null;
                        }
                    }
                };


                //adding our stringrequest to queue
                Volley.newRequestQueue(RegisterUserActivity.this).add(stringRequest);
            }
        });
    }

    private void showDateDialog() {

        Calendar newCalendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                edtTanggalLahir.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

}


