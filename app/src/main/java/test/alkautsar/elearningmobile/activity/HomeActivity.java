package test.alkautsar.elearningmobile.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.fragment.HistoryFragment;
import test.alkautsar.elearningmobile.fragment.HomeFragment;
import test.alkautsar.elearningmobile.fragment.ModulFragment;
import test.alkautsar.elearningmobile.fragment.ProfilFragment;
import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.utils.Utils;

public class HomeActivity extends AppCompatActivity {

    private CardView materi, profile, ebook, video, engagement, nilai, logout;
    private TextView txtNamaUser, txtEmailUser, judulCampaign;
    private String json, file;
    private SliderLayout sliderLayout;
    private List<SliderLayout> list_dataList;
    private LinearLayout mLinearLayout;
    HashMap<String, String> Hash_file_maps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        materi = findViewById(R.id.cvMateri);
        profile = findViewById(R.id.cvProfile);
        ebook = findViewById(R.id.cvEbook);
        video = findViewById(R.id.cvVideo);
        engagement = findViewById(R.id.cvEngagement);
        nilai = findViewById(R.id.cvNilai);
        txtNamaUser = findViewById(R.id.txtUserName);
        txtEmailUser = findViewById(R.id.txtEmailUser);
        logout = findViewById(R.id.cvLogout);

        sliderLayout = (com.daimajia.slider.library.SliderLayout) findViewById(R.id.slider);
        judulCampaign = findViewById(R.id.lblJudulCampaign);
        mLinearLayout = (LinearLayout) findViewById(R.id.pagesContainer);

        getPromotion();

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(HomeActivity.this)
                        .setTitle("Logout")
                        .setMessage("Apakah anda mau Logout?")
                        .setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setPositiveButton("IYA", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {
                                SessionManager sessionManager = new SessionManager(getApplicationContext());
                                sessionManager.logoutUser();
                            }
                        }).create().show();
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
            }
        });

        materi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, MateriActivity.class));
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, MateriVidioActivity.class));
            }
        });

        ebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, EbookActivity.class));
            }
        });

        engagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, EngagementActivity.class));
            }
        });

        nilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");*/
                startActivity(new Intent(HomeActivity.this, NilaiUserActivity.class));
            }
        });
    }

    private void getPromotion() {

        SessionManager sessionManager = new SessionManager(HomeActivity.this.getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        String nik = user.get(SessionManager.KEY_NIK);
        String namaUser = user.get(SessionManager.KEY_NAME);
        String emailUser = user.get(SessionManager.KEY_EMAIL);
        System.out.println("TOKEN = " + token);

        txtNamaUser.setText(namaUser);
        txtEmailUser.setText(emailUser);

        Hash_file_maps = new HashMap<String, String>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "promosi",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            System.out.println("message = " + message);
                            String path = jresp.getString("path");
                            String data = jresp.getString("data");
                            JSONArray dataUser = new JSONArray(data);
                            for (int i = 0; i < dataUser.length(); i++) {
                                JSONObject jData = dataUser.getJSONObject(i);
                                file = jData.getString("file");
                                String jdul = jData.getString("nama");
                                String pathImage = path + file;
                                System.out.println("IMAGE = " + pathImage);
                                System.out.println("JUDUL = " + jdul);

                                Hash_file_maps.put(jdul, pathImage);

                            }

                            for (String name : Hash_file_maps.keySet()) {

                                TextSliderView textSliderView = new TextSliderView(HomeActivity.this);
                                textSliderView
                                        .description(name)
                                        .image(Hash_file_maps.get(name))
                                        .setScaleType(BaseSliderView.ScaleType.Fit)
                                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                            @Override
                                            public void onSliderClick(BaseSliderView slider) {
                                                judulCampaign.setText(slider.getBundle().get("extra").toString());
                                                System.out.println("JUDUL CAMPAIGN = " + judulCampaign.getText().toString().replace(" ", "_"));

                                            }
                                        });
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle()
                                        .putString("extra", name);
                                sliderLayout.addSlider(textSliderView);
                            }
                            sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(new DescriptionAnimation());
                            sliderLayout.setDuration(5000);
                            sliderLayout.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                }

                                @Override
                                public void onPageSelected(int position) {
                                    Log.d("Slider Demo", "Page Changed: " + position);
                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {

                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(HomeActivity.this.getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(HomeActivity.this.getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(HomeActivity.this).add(stringRequest);
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
        alertDialog.setTitle("BPR KREDIT MANDIRI");
        alertDialog.setIcon(R.drawable.logobpr);
        alertDialog.setMessage("Keluar dari Aplikasi?");
        alertDialog.setIcon(R.drawable.logobpr);
        alertDialog.setPositiveButton("IYA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                HomeActivity.super.onBackPressed();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
                startActivity(intent);
                finish();
                System.exit(0);
            }
        });

        alertDialog.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).create().show();

    }
}