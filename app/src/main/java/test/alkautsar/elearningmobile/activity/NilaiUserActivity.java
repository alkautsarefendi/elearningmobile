package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.adapter.NilaiAdapter;
import test.alkautsar.elearningmobile.model.ImageMateriModel;
import test.alkautsar.elearningmobile.model.NilaiModels;
import test.alkautsar.elearningmobile.utils.Utils;

public class NilaiUserActivity extends AppCompatActivity {

    RecyclerView rvNilai;
    SwipeRefreshLayout swipeRefreshLayout;
    String json;
    private List<NilaiModels> nilaiModels;
    NilaiModels nm;
    private NilaiAdapter nilaiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nilai_user);

        nilaiModels = new ArrayList<>();

        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh
                swipeRefreshLayout.setRefreshing(false);
                loadNilai();
            }
        });
        rvNilai = findViewById(R.id.rv_nilai);

        rvNilai.setHasFixedSize(true);
        rvNilai.setLayoutManager(new LinearLayoutManager(NilaiUserActivity.this, LinearLayoutManager.VERTICAL, false));
        
        loadNilai();

    }

    private void loadNilai() {

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        String idUser = user.get(SessionManager.KEY_ID_USER);
        System.out.println("TOKEN = " + token);
        System.out.println("IDUSER = " + idUser);

        try {
            JSONObject obj = new JSONObject();
            obj.put("id", idUser);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "getnilai",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            System.out.println("message = " + message);
                            String path = jresp.getString("path");
                            String data = jresp.getString("data");
                            JSONArray dataArray = new JSONArray(data);
                            for (int i = 0; i < dataArray.length(); i++){
                                JSONObject dataJson = dataArray.getJSONObject(i);
                                int idNilai = dataJson.getInt("id");
                                int id_materi = dataJson.getInt("id_materi");
                                String type_test = dataJson.getString("type_test");
                                String nilai_test = dataJson.getString("nilai_test");
                                String tgl_mulai = dataJson.getString("tgl_mulai");
                                String jam_mulai = dataJson.getString("jam_mulai");
                                String tgl_selesai = dataJson.getString("tgl_selesai");
                                String jam_selesai = dataJson.getString("jam_selesai");
                                String jml_wkt_finish = dataJson.getString("jml_wkt_finish");
                                String judul_materi = dataJson.getString("judul_materi");
                                String img_materi = dataJson.getString("img_materi");
                                String type_materi = dataJson.getString("type_materi");

                                Log.e("IDNILAI", String.valueOf(idNilai));

                                nm = new NilaiModels();
                                nm.setIdNilai(String.valueOf(idNilai));
                                nm.setId_materi(String.valueOf(id_materi));
                                nm.setType_test(type_test);
                                nm.setNilai_test(nilai_test);
                                nm.setTgl_mulai(tgl_mulai);
                                nm.setJam_mulai(jam_mulai);
                                nm.setTgl_selesai(tgl_selesai);
                                nm.setJam_selesai(jam_selesai);
                                nm.setJml_wkt_finish(jml_wkt_finish);
                                nm.setJudul_materi(judul_materi);
                                nm.setImg_materi(img_materi);
                                nm.setType_materi(type_materi);
                                nm.setPath(path);
                                nilaiModels.add(nm);
                            }

                            nilaiAdapter = new NilaiAdapter(NilaiUserActivity.this, nilaiModels);
                            rvNilai.setAdapter(nilaiAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(NilaiUserActivity.this).add(stringRequest);
    }
}