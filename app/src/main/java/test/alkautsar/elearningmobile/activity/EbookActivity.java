package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.adapter.EbookAdapter;
import test.alkautsar.elearningmobile.adapter.MateriAdapter;
import test.alkautsar.elearningmobile.model.DataEbook;
import test.alkautsar.elearningmobile.model.DataMateri;
import test.alkautsar.elearningmobile.utils.SpacingItemDecoration;
import test.alkautsar.elearningmobile.utils.Tools;
import test.alkautsar.elearningmobile.utils.Utils;

public class EbookActivity extends AppCompatActivity {

    SwipeRefreshLayout swipeRefreshLayout;
    private List<DataEbook> data_ebook;
    String json;
    RecyclerView rv_ebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebook);

        rv_ebook = findViewById(R.id.rv_ebook);

        data_ebook = new ArrayList<>();

        rv_ebook.setLayoutManager(new GridLayoutManager(this, 2));
        rv_ebook.addItemDecoration(new SpacingItemDecoration(2, Tools.dpToPx(this, 10), true));
        rv_ebook.setHasFixedSize(true);

        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancel the Visual indication of a refresh
                swipeRefreshLayout.setRefreshing(false);
                loadEbook();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadEbook();
    }

    private void loadEbook() {

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        System.out.println("TOKEN = "+token);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header)+"ebook",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        data_ebook.clear();
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            String path = jresp.getString("path");

                            if (message.equals("Success")){
                                String jArray = jresp.getString("data");
                                JSONArray array = new JSONArray(jArray);
                                for (int i = 0; i < array.length(); i++){
                                    JSONObject data = array.getJSONObject(i);
                                    String idEbook = data.getString("id");
                                    String judulEbook = data.getString("nama");
                                    String fileCover = data.getString("cover");

                                    DataEbook de = new DataEbook();
                                    de.setIdEbook(idEbook);
                                    de.setJudul_ebook(judulEbook);
                                    de.setCover(fileCover);
                                    de.setPathEbook(path);
                                    data_ebook.add(de);
                                }

                            }



                            EbookAdapter adapter = new EbookAdapter(EbookActivity.this, data_ebook);
                            adapter.notifyDataSetChanged();
                            rv_ebook.setAdapter(adapter);
                            adapter.setOnItemClickListener(new EbookAdapter.OnItemClickListener() {
                                @Override
                                public void onMateriClick(int position) {
                                    Intent detailIntent = new Intent(getApplicationContext(), DetailEbookActivity.class);
                                    DataEbook clickedItem = data_ebook.get(position);
                                    detailIntent.putExtra("idEbook", clickedItem.getIdEbook());
                                    System.out.println("ID = " + clickedItem.getIdEbook());

                                    startActivity(detailIntent);
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(),"Gagal Mengambil Data!"+e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(),"Gagal Mengambil Data!");
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  "+token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee){
                    return null;
                }
            }
        };



        //adding our stringrequest to queue
        Volley.newRequestQueue(EbookActivity.this).add(stringRequest);

    }

    public void goBack(View view) {
        startActivity(new Intent(EbookActivity.this, HomeActivity.class));
    }
}