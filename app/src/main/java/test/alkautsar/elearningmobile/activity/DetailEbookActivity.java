package test.alkautsar.elearningmobile.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.model.ImageMateriModel;
import test.alkautsar.elearningmobile.utils.Utils;

public class DetailEbookActivity extends AppCompatActivity {

    public static final String EXTRA_ID_EBOOK = "idEbook";
    ImageView imgEbookDetail;
    TextView tv_ebookDetail;
    Button btnNextMateriDetail;
    private String idEbook, json;
    private List<ImageMateriModel> img_materi;
    private ArrayAdapter adapter;
    private int x, arrImg;
    ImageMateriModel im ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ebook);

        img_materi = new ArrayList<>();

        Intent intent = getIntent();
        idEbook = intent.getStringExtra(EXTRA_ID_EBOOK);
        System.out.println("idEbook = " + idEbook);

        imgEbookDetail = findViewById(R.id.imgEbookDetail);
        tv_ebookDetail = findViewById(R.id.txtHeaderDetailMateri);
        btnNextMateriDetail = findViewById(R.id.btnNextEbookDetail);
        btnNextMateriDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setImgMateri();
            }
        });

        loadEbookDetail();
    }

    private void setImgMateri() {
        System.out.println("Banyak MATERI = " + img_materi.size());
        arrImg = img_materi.size();

        if (x >= arrImg) {
            Intent i = new Intent(getApplicationContext(), EbookActivity.class);
            startActivity(i);
        } else {
            im = img_materi.get(x);
            System.out.println("KANCIANG = "+im.getPath()+im.getImgMateri());
            Glide.with(this)
                    .load(im.getPath()+im.getImgMateri())
                    .into(imgEbookDetail);

        }
        x++;
    }

    private void loadEbookDetail() {

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        System.out.println("TOKEN = " + token);

        try {
            JSONObject obj = new JSONObject();
            obj.put("id", idEbook);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "ebookbyid",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            System.out.println("message = " + message);
                            String path = jresp.getString("path");
                            String jArray = jresp.getString("data");
                            JSONArray jsonArray = new JSONArray(jArray);
                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject imgJSON = jsonArray.getJSONObject(i);
                                String fileImage = imgJSON.getString("file");


                                im = new ImageMateriModel();
                                im.setImgMateri(fileImage);
                                im.setPath(path);
                                img_materi.add(im);
                            }
                            adapter = new ArrayAdapter<>(DetailEbookActivity.this,R.layout.support_simple_spinner_dropdown_item,img_materi);
                            adapter.notifyDataSetChanged();
                            setImgMateri();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(DetailEbookActivity.this).add(stringRequest);
    }
}