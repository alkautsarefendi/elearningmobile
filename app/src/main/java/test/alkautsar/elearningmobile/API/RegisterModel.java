package test.alkautsar.elearningmobile.API;

public class RegisterModel {

    private String nama;
    private String email;
    private String password;
    private String nik;
    private String divisi;
    private String jabatan;
    private String cabang;
    private String level;
    private String tglLahir;

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public void setCabang(String cabang) {
        this.cabang = cabang;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getNik() {
        return nik;
    }

    public String getDivisi() {
        return divisi;
    }

    public String getJabatan() {
        return jabatan;
    }

    public String getCabang() {
        return cabang;
    }

    public String getLevel() {
        return level;
    }

    public String getTglLahir() {
        return tglLahir;
    }
}
