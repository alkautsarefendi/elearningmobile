package test.alkautsar.elearningmobile.API;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface APIService {

    @FormUrlEncoded
    @POST("register")
    Call<Result> createUser(
            @Field("name") String nama,
            @Field("email_user") String email,
            @Field("password") String password,
            @Field("nik") String nik,
            @Field("divisi") String divisi,
            @Field("jabatan") String jabatan,
            @Field("cabang") String cabang,
            @Field("level") String level,
            @Field("tgl_lahir") String tglLahir);

    @FormUrlEncoded
    @POST("login")
    Call<Result> login(
            @Field("email") String nik,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("materi")
    Call<Result> materiAll(
            @Header("Authorization") String token,
            @Field("divisi") String divisi,
            @Field("type") String type);
}
