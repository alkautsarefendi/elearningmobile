package test.alkautsar.elearningmobile.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.SessionManager;
import test.alkautsar.elearningmobile.activity.EbookActivity;
import test.alkautsar.elearningmobile.activity.LoginActivity;
import test.alkautsar.elearningmobile.activity.MateriActivity;
import test.alkautsar.elearningmobile.activity.MateriVidioActivity;
import test.alkautsar.elearningmobile.model.ImageMateriModel;
import test.alkautsar.elearningmobile.utils.Utils;

public class HomeFragment extends Fragment {

    private CardView materi, softskill, ebook, video, engagement, nilai;
    private TextView txtNamaUser, txtEmailUser, judulCampaign;
    private String json, file;
    private SliderLayout sliderLayout;
    private List<SliderLayout> list_dataList;
    private LinearLayout mLinearLayout;
    HashMap<String, String> Hash_file_maps;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        materi = view.findViewById(R.id.cvMateri);
        softskill = view.findViewById(R.id.cvMateriSoft);
        ebook = view.findViewById(R.id.cvEbook);
        video = view.findViewById(R.id.cvVideo);
        engagement = view.findViewById(R.id.cvEngagement);
        nilai = view.findViewById(R.id.cvNilai);
        txtNamaUser = view.findViewById(R.id.txtUserName);
        txtEmailUser = view.findViewById(R.id.txtEmailUser);

        sliderLayout = (com.daimajia.slider.library.SliderLayout) view.findViewById(R.id.slider);
        judulCampaign = view.findViewById(R.id.lblJudulCampaign);
        mLinearLayout = (LinearLayout) view.findViewById(R.id.pagesContainer);

        userDetail();
        getPromotion();

        materi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MateriActivity.class));
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MateriVidioActivity.class));
            }
        });

        ebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EbookActivity.class));
            }
        });

        return view;
    }

    private void getPromotion() {

        SessionManager sessionManager = new SessionManager(getActivity().getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        String nik = user.get(SessionManager.KEY_NIK);
        System.out.println("TOKEN = " + token);

        Hash_file_maps = new HashMap<String, String>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "promosi",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            System.out.println("message = " + message);
                            String path = jresp.getString("path");
                            String data = jresp.getString("data");
                            JSONArray dataUser = new JSONArray(data);
                            for (int i = 0; i < dataUser.length(); i++){
                                JSONObject jData = dataUser.getJSONObject(i);
                                file = jData.getString("file");
                                String jdul = jData.getString("nama");
                                String pathImage = path + file;
                                System.out.println("IMAGE = " + pathImage);
                                System.out.println("JUDUL = " + jdul);

                                Hash_file_maps.put(jdul,pathImage);

                            }

                            for (String name : Hash_file_maps.keySet()) {

                                TextSliderView textSliderView = new TextSliderView(getActivity());
                                textSliderView
                                        .description(name)
                                        .image(Hash_file_maps.get(name))
                                        .setScaleType(BaseSliderView.ScaleType.Fit)
                                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                            @Override
                                            public void onSliderClick(BaseSliderView slider) {
                                                judulCampaign.setText(slider.getBundle().get("extra").toString());
                                                System.out.println("JUDUL CAMPAIGN = " + judulCampaign.getText().toString().replace(" ", "_"));

                                            }
                                        });
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle()
                                        .putString("extra", name);
                                sliderLayout.addSlider(textSliderView);
                            }
                            sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(new DescriptionAnimation());
                            sliderLayout.setDuration(5000);
                            sliderLayout.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                }

                                @Override
                                public void onPageSelected(int position) {
                                    Log.d("Slider Demo", "Page Changed: " + position);
                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {

                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getActivity().getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getActivity().getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }



    private void userDetail() {
        SessionManager sessionManager = new SessionManager(getActivity().getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String token = user.get(SessionManager.KEY_TOKEN);
        String nik = user.get(SessionManager.KEY_NIK);
        System.out.println("TOKEN = " + token);

        try {
            JSONObject obj = new JSONObject();
            obj.put("nik", nik);

            json = obj.toString(2);
            Log.d("output", obj.toString(2));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, getString(R.string.url_header) + "userdetail",
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object

                            Log.d("strrrrr", ">>" + response);

                            JSONObject jObj = new JSONObject(response);
                            String resp = jObj.getString("response");
                            JSONObject jresp = new JSONObject(resp);
                            String message = jresp.getString("message");
                            System.out.println("message = " + message);
                            String data = jresp.getString("data");
                            JSONArray dataUser = new JSONArray(data);
                            for (int i = 0; i < dataUser.length(); i++){
                                JSONObject jData = dataUser.getJSONObject(i);
                                String namaUser = jData.getString("name");
                                String emailUser = jData.getString("email_user");
                                String cabUser = jData.getString("nm_cabang");
                                String divUser = jData.getString("nm_divisi");
                                String jabUser = jData.getString("nm_jabatan");

                                /*System.out.println("NAMA = " + namaUser);
                                System.out.println("EMAIL = " + emailUser);
                                System.out.println("CABANG = " + cabUser);
                                System.out.println("DIVISI = " + divUser);
                                System.out.println("JABATAN = " + jabUser);*/


                            }

                            SessionManager sessionManager = new SessionManager(getActivity().getApplicationContext());
                            HashMap<String, String> user = sessionManager.getUserDetails();
                            String nama = user.get(SessionManager.KEY_NAME);
                            String email = user.get(SessionManager.KEY_EMAIL);

                            txtNamaUser.setText(nama);
                            txtEmailUser.setText(email);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.toastAlert(getActivity().getApplicationContext(), "Gagal Mengambil Data!" + e);
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.toastAlert(getActivity().getApplicationContext(), "Gagal Mengambil Data!");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer  " + token);
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return json == null ? null : json.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }



}