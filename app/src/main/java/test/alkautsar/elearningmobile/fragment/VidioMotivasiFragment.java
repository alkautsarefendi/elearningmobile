package test.alkautsar.elearningmobile.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import test.alkautsar.elearningmobile.R;

public class VidioMotivasiFragment extends Fragment {

    public static VidioMotivasiFragment newInstance() {
        VidioMotivasiFragment fragment = new VidioMotivasiFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vidio_motivasi, container, false);
    }
}