package test.alkautsar.elearningmobile.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.model.NilaiModels;

public class NilaiAdapter extends RecyclerView.Adapter<NilaiAdapter.NilaiViewHolder> {

    private Context mCtx;
    private List<NilaiModels> models;
    private OnItemClickListener mListener;
    private RecyclerView currentItem;
    private String x, hasil;

    public interface OnItemClickListener {
        void onDonasiClick(int position);

        void onDetailClick(int position);

        void onImageClick(int position);

        void onJudulClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public NilaiAdapter(Context mCtx, List<NilaiModels> models) {
        this.mCtx = mCtx;
        this.models = models;
    }

    @Override
    public NilaiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.row_nilai, null);
        return new NilaiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NilaiViewHolder holder, int position) {
        NilaiModels materi = models.get(position);

        String pathGambar = materi.getPath();
        String idmateri = materi.getId_materi();
        String loadImgae = pathGambar+"/"+idmateri+"/"+materi.getImg_materi();
        Log.e("IMAGE COVER", loadImgae);
        //loading the image
        Glide.with(mCtx)
                .load(loadImgae)
                .into(holder.imgCover);

        holder.judulMateri.setText(materi.getJudul_materi());
        holder.typeMateri.setText(materi.getType_materi());
        holder.nilaiTest.setText(materi.getNilai_test());
//        holder.lamaTest.setText(materi.getJml_wkt_finish());
        holder.jenisTest.setText(materi.getType_test());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class NilaiViewHolder extends RecyclerView.ViewHolder {

        TextView judulMateri, typeMateri, nilaiTest, lamaTest, jenisTest;
        ImageView imgCover;

        public NilaiViewHolder(View itemView) {
            super(itemView);

            judulMateri = itemView.findViewById(R.id.txtJudulMateriNilai);
            typeMateri = itemView.findViewById(R.id.txtTypeMateri);
            nilaiTest = itemView.findViewById(R.id.txtNilaiTest);
//            lamaTest = itemView.findViewById(R.id.txtLamaTest);
            imgCover = itemView.findViewById(R.id.imgCover);
            jenisTest = itemView.findViewById(R.id.txtJenisTest);

        }

        public void filterList(ArrayList<NilaiModels> filteredList) {
            models.clear();
            models.addAll(filteredList);
            //productList = filteredList;
            notifyDataSetChanged();
        }

    }
}
