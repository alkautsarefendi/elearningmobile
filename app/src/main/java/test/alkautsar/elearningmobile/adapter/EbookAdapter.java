package test.alkautsar.elearningmobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.model.DataEbook;
import test.alkautsar.elearningmobile.model.DataMateri;

public class EbookAdapter extends RecyclerView.Adapter<EbookAdapter.EbookViewHolder> {

    private Context mCtx;
    private List<DataEbook> dataEbook;
    private OnItemClickListener mListener;
    private RecyclerView currentItem;
    private String x, hasil;

    public interface OnItemClickListener {
        void onMateriClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public EbookAdapter(Context mCtx, List<DataEbook> dataEbook) {
        this.mCtx = mCtx;
        this.dataEbook = dataEbook;
    }

    @Override
    public EbookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.row_materi, null);
        return new EbookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EbookViewHolder holder, int position) {
        DataEbook ebook = dataEbook.get(position);
        //loading the image
        String coverEbbok = ebook.getPathEbook()+ebook.getCover();
        System.out.println("GAMBAR = "+coverEbbok);
        if (coverEbbok.isEmpty()){
            holder.gambarMateri.setImageResource(R.drawable.bg_no_item_city);
        } else {
            Glide.with(mCtx)
                    .load(coverEbbok)
                    .into(holder.gambarMateri);
        }
        holder.namaMateri.setText(ebook.getJudul_ebook());
    }

    @Override
    public int getItemCount() {
        return dataEbook.size();
    }

    class EbookViewHolder extends RecyclerView.ViewHolder {

        TextView namaMateri;
        ImageView gambarMateri;
        Button btnPilih;

        public EbookViewHolder(View itemView) {
            super(itemView);

            namaMateri = itemView.findViewById(R.id.judulCampaign);
            gambarMateri = itemView.findViewById(R.id.imgMateri);
            btnPilih = itemView.findViewById(R.id.btnPilihMateri);

            btnPilih.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onMateriClick(position);
                        }
                    }
                }
            });

        }

    }

    public void filterList(ArrayList<DataEbook> filteredList) {
        dataEbook.clear();
        dataEbook.addAll(filteredList);
        //productList = filteredList;
        notifyDataSetChanged();
    }

    private static long daysBetween(Calendar tanggalAwal, Calendar tanggalAkhir) {
        long lama = 0;
        Calendar tanggal = (Calendar) tanggalAwal.clone();
        while (tanggal.before(tanggalAkhir)) {
            tanggal.add(Calendar.DAY_OF_MONTH, 1);
            lama++;
        }
        return lama;
    }
}
