package test.alkautsar.elearningmobile.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import test.alkautsar.elearningmobile.R;
import test.alkautsar.elearningmobile.activity.DataVideoM;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder>{

    private Context mCtx;
    private List<DataVideoM> dataVideoMS;
    private MateriAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onMateriClick(int position);
    }

    public void setOnItemClickListener(MateriAdapter.OnItemClickListener listener) {
        mListener = listener;
    }


    public VideoAdapter(Context mCtx, List<DataVideoM> dataVideoMS) {
        this.mCtx = mCtx;
        this.dataVideoMS = dataVideoMS;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.row_materi, null);
        return new VideoViewHolder(view);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(VideoViewHolder holder, int position) {
        DataVideoM video = dataVideoMS.get(position);
        //loading the image
        String thunbnail = video.getPath()+video.getFile();
        System.out.println("VIDEO = "+thunbnail);
        if (thunbnail.isEmpty()){
            holder.gambarMateri.setImageResource(R.drawable.bg_no_item_city);
        } else {
            String pathGambar = video.getPath();
            System.out.println("PATH = "+pathGambar);
            Glide.with(mCtx)
                    .load(thunbnail)
                    .centerCrop()
                    .placeholder(Color.BLUE)
                    .into(holder.gambarMateri);
        }
        holder.namaMateri.setText(video.getJudulVideoM());
    }

    @Override
    public int getItemCount() {
        return dataVideoMS.size();
    }

    class VideoViewHolder extends RecyclerView.ViewHolder {

        TextView namaMateri;
        ImageView gambarMateri;
        Button btnPilih;

        public VideoViewHolder(View itemView) {
            super(itemView);

            namaMateri = itemView.findViewById(R.id.judulCampaign);
            gambarMateri = itemView.findViewById(R.id.imgMateri);
            btnPilih = itemView.findViewById(R.id.btnPilihMateri);

            btnPilih.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onMateriClick(position);
                        }
                    }
                }
            });

        }

    }

    public void filterList(ArrayList<DataVideoM> filteredList) {
        dataVideoMS.clear();
        dataVideoMS.addAll(filteredList);
        //productList = filteredList;
        notifyDataSetChanged();
    }

    private static long daysBetween(Calendar tanggalAwal, Calendar tanggalAkhir) {
        long lama = 0;
        Calendar tanggal = (Calendar) tanggalAwal.clone();
        while (tanggal.before(tanggalAkhir)) {
            tanggal.add(Calendar.DAY_OF_MONTH, 1);
            lama++;
        }
        return lama;
    }
}
