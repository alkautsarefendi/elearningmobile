package test.alkautsar.elearningmobile.model;

public class SliderModel {

    private String imageurl;

    public SliderModel(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getImageurl() {
        return imageurl;
    }
}
