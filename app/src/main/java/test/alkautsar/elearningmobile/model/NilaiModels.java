package test.alkautsar.elearningmobile.model;

public class NilaiModels {
    String idNilai, id_materi, type_test, nilai_test,
            tgl_mulai, jam_mulai, tgl_selesai, jam_selesai,
            jml_wkt_finish, judul_materi, img_materi, type_materi, path;

    public String getIdNilai() {
        return idNilai;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setIdNilai(String idNilai) {
        this.idNilai = idNilai;
    }

    public String getId_materi() {
        return id_materi;
    }

    public void setId_materi(String id_materi) {
        this.id_materi = id_materi;
    }

    public String getType_test() {
        return type_test;
    }

    public void setType_test(String type_test) {
        this.type_test = type_test;
    }

    public String getNilai_test() {
        return nilai_test;
    }

    public void setNilai_test(String nilai_test) {
        this.nilai_test = nilai_test;
    }

    public String getTgl_mulai() {
        return tgl_mulai;
    }

    public void setTgl_mulai(String tgl_mulai) {
        this.tgl_mulai = tgl_mulai;
    }

    public String getJam_mulai() {
        return jam_mulai;
    }

    public void setJam_mulai(String jam_mulai) {
        this.jam_mulai = jam_mulai;
    }

    public String getTgl_selesai() {
        return tgl_selesai;
    }

    public void setTgl_selesai(String tgl_selesai) {
        this.tgl_selesai = tgl_selesai;
    }

    public String getJam_selesai() {
        return jam_selesai;
    }

    public void setJam_selesai(String jam_selesai) {
        this.jam_selesai = jam_selesai;
    }

    public String getJml_wkt_finish() {
        return jml_wkt_finish;
    }

    public void setJml_wkt_finish(String jml_wkt_finish) {
        this.jml_wkt_finish = jml_wkt_finish;
    }

    public String getJudul_materi() {
        return judul_materi;
    }

    public void setJudul_materi(String judul_materi) {
        this.judul_materi = judul_materi;
    }

    public String getImg_materi() {
        return img_materi;
    }

    public void setImg_materi(String img_materi) {
        this.img_materi = img_materi;
    }

    public String getType_materi() {
        return type_materi;
    }

    public void setType_materi(String type_materi) {
        this.type_materi = type_materi;
    }
}
