package test.alkautsar.elearningmobile.model;

public class ImageMateriModel {
    private String imgMateri, path;

    public String getImgMateri() {
        return imgMateri;
    }

    public void setImgMateri(String imgMateri) {
        this.imgMateri = imgMateri;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
