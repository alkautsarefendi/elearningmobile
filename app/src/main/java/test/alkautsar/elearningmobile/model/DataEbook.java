package test.alkautsar.elearningmobile.model;

public class DataEbook {

    String idEbook,judul_ebook, cover, pathEbook;

    public String getIdEbook() {
        return idEbook;
    }

    public void setIdEbook(String idEbook) {
        this.idEbook = idEbook;
    }

    public String getJudul_ebook() {
        return judul_ebook;
    }

    public void setJudul_ebook(String judul_ebook) {
        this.judul_ebook = judul_ebook;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getPathEbook() {
        return pathEbook;
    }

    public void setPathEbook(String pathEbook) {
        this.pathEbook = pathEbook;
    }
}
