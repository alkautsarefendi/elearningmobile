package test.alkautsar.elearningmobile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import test.alkautsar.elearningmobile.activity.HomeActivity;
import test.alkautsar.elearningmobile.activity.LoginActivity;

public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context mContext;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "BPRPref";
    private static final String IS_LOGIN = "isLogin";
    public static final String KEY_NIK = "nik";
    public static final String KEY_NAME = "name";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_DIVISI = "divisi";
    public static final String KEY_CABANG = "cabang";
    public static final String KEY_JABATAN = "jabatan";
    public static final String KEY_TGL_LAHIR = "tglLahir";
    public static final String KEY_ID_USER = "idUser";

    public SessionManager(Context context){
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String nik, String token){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NIK, nik);
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public void createDetailUser(String nama, String email, String cabang, String divisi, String jabtan, String tgl_lahir, String nik_user, String id_user){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, nama);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_CABANG, cabang);
        editor.putString(KEY_DIVISI, divisi);
        editor.putString(KEY_JABATAN, jabtan);
        editor.putString(KEY_TGL_LAHIR, tgl_lahir);
        editor.putString(KEY_NIK,nik_user);
        editor.putString(KEY_ID_USER, id_user);
        editor.commit();
    }

    public void checkLogin(){
        if (!this.isLogin()){
            Intent i = new Intent(mContext, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        } else {
            Intent i = new Intent(mContext, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        }
    }

    public HashMap<String, String > getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_NIK, pref.getString(KEY_NIK, null));
        user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_CABANG, pref.getString(KEY_CABANG, null));
        user.put(KEY_DIVISI, pref.getString(KEY_DIVISI, null));
        user.put(KEY_JABATAN, pref.getString(KEY_JABATAN, null));
        user.put(KEY_TGL_LAHIR, pref.getString(KEY_TGL_LAHIR,null));
        user.put(KEY_ID_USER, pref.getString(KEY_ID_USER, null));
        return user;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        Intent i = new Intent(mContext, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }

    private boolean isLogin() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}
